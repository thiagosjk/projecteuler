<?php
	
	$size = 80;
	$array;
	$pesos;
	$infinite = 99999999999999;

	//setupTesteArray();
	setupArray();

	printArray($array, $size);
	$pesos = iniciaArray($pesos, $size);

	


	for($i=0;$i<=$size;$i++){
		$max += $array[0][$i];
		$max += $array[$size-1][$i];
	}
	$max -= $array[0][$size-1];

	$aux = 0;
	for($i=1;$i<=$size;$i++){
		for($j=1;$j<=$size;$j++){
			$min = min($pesos[$i-1][$j], $pesos[$i][$j-1]);
			$pesos[$i][$j] = $array[$i][$j] + (($min == $infinite) ? 0 : $min);
		}

	}

	printArray($pesos, $size);



function setupTesteArray(){
	global $array;
	global $size;
	global $infinite;
	$size = 5;

	for($i=0;$i<=$size;$i++)
	{
		$array[$i][0] = $infinite;
		$array[0][$i] = $infinite;
	}

	$array[1][1] = 131;
	$array[1][2] = 201;
	$array[1][3] = 630;
	$array[1][4] = 537;
	$array[1][5] = 805;

	$array[2][1] = 673;
	$array[2][2] = 96;
	$array[2][3] = 803;
	$array[2][4] = 699;
	$array[2][5] = 732;

	$array[3][1] = 234;
	$array[3][2] = 342;
	$array[3][3] = 746;
	$array[3][4] = 497;
	$array[3][5] = 524;

	$array[4][1] = 103;
	$array[4][2] = 965;
	$array[4][3] = 422;
	$array[4][4] = 121;
	$array[4][5] = 37;

	$array[5][1] = 18;
	$array[5][2] = 150;
	$array[5][3] = 111;
	$array[5][4] = 956;
	$array[5][5] = 331;
}

function setupArray(){
	global $array;
	global $size;
	global $infinite;

	$array = iniciaArray($array, $size);

	$lines = file ('https://projecteuler.net/project/resources/p081_matrix.txt');
	$i = $j = 1;
	foreach ($lines as $line_num => $line) {
		$j = 1;
		$elementos = explode(',', $line);
		foreach ($elementos as $num) {
			$array[$i][$j] = $num;
			$j++;
		}
		$i++;
	}
}	

function printArray($array, $size){
	for ($i=0; $i <= $size; $i++) { 
		for ($j=0; $j <= $size; $j++) { 
			echo $array[$j][$i]."\t";
		}
		echo "\n";
	}
	echo "\n\n";
}

function iniciaArray($array, $size){
	global $infinite;

	for($i=0;$i<=$size;$i++)
	{
		$array[$i][0] = $infinite;
		$array[0][$i] = $infinite;
	}

	for ($i=1; $i <= $size; $i++) { 
		for ($j=1; $j <= $size; $j++) { 
			$array[$i][$j] = 0;
		}
	}

	return $array;
}