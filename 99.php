<?php

	$lines = file ('https://projecteuler.net/project/resources/p099_base_exp.txt');

	$i=1;
	$maior = 0;
	$final = 0;
	foreach ($lines as $line_num => $line) {
	    $values = explode(",", $line);
	    $aux = $values[1] * log($values[0]);
		

		if($aux > $maior){
			$maior = $aux;
			$final = $i;
		}

		echo $i++ . ": " . $aux . "\n";
	}

	echo "\n\n\n" . $final;